import axiosLib from 'axios';
const axios = axiosLib.create({
  baseURL: process.env.REACT_APP_API_BASE_PATH
});

export default axios;
