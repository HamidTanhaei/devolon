export const categoriesMockData = [
  {
    id: 5,
    name: 'boxes'
  },
  {
    id: 15,
    name: 'clothes'
  },
  {
    id: 1,
    name: 'hats'
  }
];

export const expectedActions = [
  {
    type: 'READ_RESOURCES_PENDING',
    resourceType: 'imageCategories',
    requestKey: 'getImageCategories',
    list: 'allCategories'
  },
  {
    type: 'READ_RESOURCES_SUCCEEDED',
    resourceType: 'imageCategories',
    requestKey: 'getImageCategories',
    resources: categoriesMockData,
    list: 'allCategories'
  }
];
