import configureMockStore from 'redux-mock-store'
import MockAdapter from 'axios-mock-adapter';
import thunk from 'redux-thunk'
import expect from 'expect'
import {axios} from '../../instances';
import {getImageCategories} from './index';
import {expectedActions, categoriesMockData} from './testData';
import appReducer from '../rootReducer';

const mockAxios = new MockAdapter(axios);
const middlewares = [thunk]
const mockStore = configureMockStore(middlewares);

const initialStatee = {imageCategories:{resources:{},meta:{},requests:{},lists:{},resourceType:'imageCategories'}};

describe('Redux image categories', () => {
  afterEach(() => {
    mockAxios.reset();
  })

  it('should send a request to the server and save incoming data', () => {
    const theUrl = `${process.env.REACT_APP_API_BASE_PATH}/v1/categories`;
    mockAxios.onGet(theUrl).reply(200, categoriesMockData);
    //@ts-ignore
    const createState = initialState => actions => actions.reduce(appReducer, initialState);
    const initialState = createState(initialStatee);

    const store = mockStore(initialState);

    // @ts-ignore
    return store.dispatch(((dispatch) => getImageCategories({dispatcher: dispatch}))).then(() => {
      const storeState = store.getState();
      //@ts-ignore
      expect(Object.keys(storeState.imageCategories.resources).length).toEqual(3);
      expect(store.getActions()).toEqual(expectedActions);
    })
  })
})
