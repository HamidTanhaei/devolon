import {readResources} from '../util/redux-resource-automation';
import {getImageCategories as getImageCategoriesApi} from '../../api/imageCategories';

export const getImageCategories = ({dispatcher} = {dispatcher: null}) => {
  return readResources({
    resourceType: 'imageCategories',
    requestKey: 'getImageCategories',
    list: 'allCategories',
    api: getImageCategoriesApi,
    dispatcher
  });
};
