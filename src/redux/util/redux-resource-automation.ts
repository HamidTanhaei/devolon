import { actionTypes } from 'redux-resource';
import {store, AppDispatch} from '../store';

interface IReq{
  resourceType: string;
  requestKey: string;
  list: string;
  api: (apiParams: any) => Promise<any>;
  apiParams?: any;
  resourceNormalizer?: (data: any) => Array<number | {}>
  ids?: number[],
  onStart?: (dispatch: AppDispatch) => void,
  dispatcher?: any
}

interface IReqMiddleware extends IReq{
  reqType: 'READ' | 'CREATE' | 'UPDATE' | 'DELETE';
}

export const reqMiddleware = ({
  reqType,
  resourceType,
  requestKey,
  list,
  api,
  apiParams= {},
  resourceNormalizer,
  ids,
  onStart,
  dispatcher,
} : IReqMiddleware): Promise<any> => {
  const dispatch = dispatcher ? dispatcher : store.dispatch

  if(onStart){
    onStart(dispatch)
  }

  let pendingPayload = {
    type: actionTypes[`${reqType}_RESOURCES_PENDING`],
    resourceType,
    requestKey,
    list
  };

  if (ids) { // @ts-ignore redux-resource should support typescript
    pendingPayload.resources = ids;
  }
  dispatch(pendingPayload);

  const apiPromise = api(apiParams);
  apiPromise
    .then((data) => {
      const successPayload = {
        type: actionTypes[`${reqType}_RESOURCES_SUCCEEDED`],
        resourceType,
        requestKey,
        resources:
          reqType === 'DELETE'
            ? ids
            : resourceNormalizer
              ? resourceNormalizer(data.data)
              : data.data,
      };
      // @ts-ignore redux-resource should support typescript
      if (list) successPayload.list = list;
      dispatch(successPayload);
      return data;
    })
    .catch((e) => {
      const rejectPayload = {
        type: actionTypes[`${reqType}_RESOURCES_FAILED`],
        resourceType,
        requestKey,
        resources: ids,
      };
      // @ts-ignore redux-resource should support typescript
      if (list) rejectPayload.list = list;
      if (ids) rejectPayload.resources = ids;
      dispatch(rejectPayload);

      return Promise.reject(e);
    });

  return apiPromise;
};
export const readResources = (req: IReq): Promise<any> => {
  return reqMiddleware({ reqType: 'READ', ...req });
};

export const createResources = (req: IReq): Promise<any> => {
  return reqMiddleware({ reqType: 'CREATE', ...req });
};

export const updateResources = (req: IReq): Promise<any> => {
  return reqMiddleware({ reqType: 'UPDATE', ...req });
};

export const deleteResources = (req: IReq): Promise<any> => {
  return reqMiddleware({ reqType: 'DELETE', ...req });
};
