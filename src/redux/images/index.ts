import { reset } from 'redux-resource-plugins';
import {readResources} from '../util/redux-resource-automation';
import {getImages as getImagesApi} from '../../api/images';

interface IReduxGetImages {
    page: number,
    categoryId: number,
    dispatcher?: null | (() => void)
}

export const getImages = ({categoryId, page = 1, dispatcher = null}: IReduxGetImages) => {
  return readResources({
    resourceType: 'images',
    requestKey: `getImage_${categoryId}_page${page}`,
    list: `cat_${categoryId}`,
    api: getImagesApi,
    apiParams: {params: {limit: 12, category_ids: categoryId, order: 'DESC', size: 'thumb', page}},
    onStart: (dispatch) => {
      if(page === 1){
        dispatch(reset.resetResource('images', {
          list: `cat_${categoryId}`,
        }))
      }
    },
    dispatcher
  });
};
