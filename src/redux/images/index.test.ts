import configureMockStore from 'redux-mock-store'
import MockAdapter from 'axios-mock-adapter';
import thunk from 'redux-thunk'
import expect from 'expect'
import {axios} from '../../instances';
import {getImages} from './index'; // You can use any testing library
import {expectedActions, imagesMockData} from './testData';
import appReducer from '../rootReducer';

const mockAxios = new MockAdapter(axios);
const middlewares = [thunk]
const mockStore = configureMockStore(middlewares);

const initialStatee = {images:{resources:{},meta:{},requests:{},lists:{},resourceType:'images'}};

describe('Redux images', () => {
  afterEach(() => {
    mockAxios.reset();
  })

  it('should send a request to the server and save incoming data', () => {
    const theUrl = `${process.env.REACT_APP_API_BASE_PATH}/v1/images/search`;
    mockAxios.onGet(theUrl).reply(200, imagesMockData);

    // @ts-ignore
    const createState = initialState => actions => actions.reduce(appReducer, initialState);
    const initialState = createState(initialStatee);

    const store = mockStore(initialState);

    // @ts-ignore
    return store.dispatch(((dispatch) => getImages({categoryId: 2, page: 1, dispatcher: dispatch}))).then((data) => {
      const storeState = store.getState();
      // @ts-ignore
      expect(Object.keys(storeState.images.resources).length).toEqual(3);
      expect(store.getActions()).toEqual(expectedActions);
    })
  })
})
