export const imagesMockData = [
  {
    'breeds': [],
    'categories': [
      {
        'id': 5,
        'name': 'boxes'
      }
    ],
    'id': 'd7',
    'url': 'https://cdn2.thecatapi.com/images/d5.png',
    'width': 600,
    'height': 588
  },
  {
    'breeds': [],
    'categories': [
      {
        'id': 5,
        'name': 'boxes'
      }
    ],
    'id': 'dq',
    'url': 'https://cdn2.thecatapi.com/images/dq.jpg',
    'width': 960,
    'height': 1280
  },
  {
    'breeds': [],
    'categories': [
      {
        'id': 5,
        'name': 'boxes'
      }
    ],
    'id': 'e8',
    'url': 'https://cdn2.thecatapi.com/images/e8.jpg',
    'width': 560,
    'height': 420
  }
];

export const expectedActions = [
  {
    type: 'RESET_RESOURCE',
    resourceType: 'images',
    list: 'cat_2'
  },
  {
    type: 'READ_RESOURCES_PENDING',
    resourceType: 'images',
    requestKey: 'getImage_2_page1',
    list: 'cat_2'
  },
  {
    type: 'READ_RESOURCES_SUCCEEDED',
    resourceType: 'images',
    requestKey: 'getImage_2_page1',
    resources: imagesMockData,
    list: 'cat_2'
  }
];
