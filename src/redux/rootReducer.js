import {combineReducers} from 'redux';
import { resourceReducer } from 'redux-resource';
import { reset } from 'redux-resource-plugins';


const reducers = {
  imageCategories: resourceReducer('imageCategories'),
  images: resourceReducer('images', {
    plugins: [reset],
  }),
};

export default combineReducers(reducers);
