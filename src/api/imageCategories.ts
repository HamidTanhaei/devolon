import {axios} from '../instances';

export const getImageCategories = () => axios.get('/v1/categories');

