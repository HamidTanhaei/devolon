import {axios} from '../instances';

interface IGetImages {
    params?: {[key: string]: string | number}
}
export const getImages = ({params = {}}: IGetImages) => axios({
  url: '/v1/images/search',
  params
});

