export interface IImageCategory {
    id: number,
    name: string
}
