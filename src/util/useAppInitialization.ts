import {useEffect} from 'react';
import {getImageCategories} from '../redux/imageCategories';

export const useAppInitialization = () => {
  useEffect(() => {
    getImageCategories();
  }, []);
}
