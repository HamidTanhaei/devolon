export const colors = {
  'primary': '#00c1ff',
  'gray': '#e9e9e9',
  'gray-dark': '#747474',
  'white': '#fff',
};

export const fontSize = {
  'base' : '14px',
  'lg' : '16px',
  'xlg' : '18px',
}

export const breakpoints = {
  md: 768,
  lg: 1280,
}
