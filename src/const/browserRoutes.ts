interface IBrowserRoutes {
  [key: string]: string | ((input: (any)) => string)
}

const browserRoutes: IBrowserRoutes = {
  'home': '/',
  'categorySingle': ({id= ':id'}) => `/category/${id}`
}

export const routeMaker = (route: string | ((input?: {}) => string), input?: {}) => {
  if(typeof route === 'string') return route;
  return input ? route(input) : route({});
}

export default browserRoutes;
