import React from 'react';
import SpinSVG from '../assets/svg/spinner.svg'

interface IProps{
  style?: React.CSSProperties
}

const Spinner: React.FC<IProps> = ({style= {width: 40, height: 40}}) => {
  return (
    <img src={SpinSVG} style={style} alt='loading' />
  );
}

export default Spinner;
