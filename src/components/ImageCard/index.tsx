import React from 'react';
import styled from 'styled-components';
import {IImage} from '../../type/IImage';
import {colors} from '../../const';

const SImageCard = styled.div`
  padding: 2px;
  border-radius: 2px;
  border: 1px solid ${colors.gray};
  background: ${colors.white};
  img{
    height: 200px;
    width: 100%;
    display: block;
    object-fit: cover;
  }
`;

const ImageCard: React.FC<IImage> = ({url, id}) => {
  return (
    <SImageCard>
      <img src={url} alt={`number ${id}`} />
    </SImageCard>
  );
}

export default ImageCard;
