import React from 'react';
import styled from 'styled-components';
import {colors} from '../../const';

interface IProps {
    onClick: () => void;
    clickable: boolean
}

const SDrawerButton = styled.div<{clickable: boolean}>`
  padding: 10px;
  font-size: 30px;
  ${({clickable}) => clickable ? `
    cursor: pointer;
      :hover{
        color: ${colors['gray-dark']};
      }  
  ` : ''}
`;

const DrawerButton: React.FC<IProps> = ({onClick, clickable}) => {
  return(
    <SDrawerButton onClick={onClick} clickable={clickable}>
      <i className='fi-menu' />
    </SDrawerButton>
  );
}

export default DrawerButton;
