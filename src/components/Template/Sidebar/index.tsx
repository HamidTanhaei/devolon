import React, {useState} from 'react';
import styled from 'styled-components';
import {getResources, getStatus} from 'redux-resource';
import {useSelector} from 'react-redux';
import { useHistory } from 'react-router-dom';
import {browserRoutes, colors} from '../../../const'
import Item from './Item'

import Spinner from '../../Spinner';
import {IImageCategory} from '../../../type/IImageCategory';
import {routeMaker} from '../../../const/browserRoutes';

const SSidebarArea = styled.div`
    width: 300px;
    background: ${colors.white};
    box-shadow: 0 0 3px ${colors['gray-dark']};
    position: fixed;
    left: 0;
    top: 54px;
    bottom: 0;
`;

const SList = styled.ul`
  padding: 0;
  margin: 0;
`;

const SLoading = styled.div`
    text-align: center;
`

const  Sidebar: React.FC = (props) => {
  const [selectedItem, setSelectedItem] = useState(0);
  let history = useHistory();

  // @ts-ignore
  const imageCategoriesReduxData = useSelector((state) => state.imageCategories);
  const imageCategoriesReduxDataStatus = getStatus(
    imageCategoriesReduxData,
    'requests.getImageCategories.status',
    true
  );

  const imageCategories: IImageCategory[] = getResources(imageCategoriesReduxData);
  
  const onSelect = (id: number) => {
    history.push(routeMaker(browserRoutes.categorySingle, {id: `${id}`}));
    setSelectedItem(id)
  };
  return (
    <SSidebarArea>
      {imageCategoriesReduxDataStatus.pending && (
        <SLoading>
          <Spinner />
        </SLoading>
      )}
      {imageCategoriesReduxDataStatus.succeeded && !!imageCategories.length && (
        <SList>
          {imageCategories.map(({id, name}) => (
            <Item key={id} onClick={() => onSelect(id)} selected={selectedItem === id}>{name}</Item>
          ))}
        </SList>
      )}
    </SSidebarArea>
  );
}

export default Sidebar;
