import React from 'react';
import styled from 'styled-components';
import {colors} from '../../../const'

const SItem = styled.li<{selected?: boolean}>`
  display: flex;
  padding: 10px 20px;
  cursor: pointer;
  ${({selected}) => selected ? `
  color:${colors.primary};
  background: ${colors.gray};
  ` : ''}
  :hover{
    background: ${colors.gray}
  }
`

const STitle = styled.span`
  margin-left: 10px;
  text-transform: capitalize;
`

interface IItemProps {
    selected?: boolean;
    onClick: () => void;
}

const Item: React.FC<IItemProps> = ({children, selected, onClick}) => {
  return(
    <SItem onClick={onClick} selected={selected}>
      <i className='fi-images'/>
      <STitle>
        {children}
      </STitle>
    </SItem>
  );
}

export default Item;
