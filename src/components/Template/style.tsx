import styled from 'styled-components';
import {breakpoints, colors, fontSize} from '../../const';

export const STemplate = styled.div`
  background: ${colors.gray};
  position: relative;
  min-height: 100vh;
`;

export const SContainer = styled.div`
    padding: 10px;
`;

export const SWrapper = styled.div<{drawerEnabled: boolean}>`
  ${({drawerEnabled}) => drawerEnabled ? `
  @media only screen and (min-width: ${breakpoints.md}px) {
    padding-left: 300px;
  }
  ` : ''}
`;

export const SHeader = styled.header`
    display: flex;
    align-items: center;
    background: ${colors.primary};
    color: ${colors.white}
`

export const SProjectTitle = styled.div`
  flex-grow: 1;
  text-align: center;
  font-size: ${fontSize.xlg};
`
