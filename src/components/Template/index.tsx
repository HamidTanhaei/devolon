import React, {useState} from 'react';
import Sidebar from './Sidebar';
import DrawerButton from './DrawerButton'
import {breakpoints} from '../../const';
import {STemplate, SHeader, SWrapper, SProjectTitle, SContainer} from './style';

const Template: React.FC<{}> = ({children}) => {
  const isLargeScreen = window.screen.width > breakpoints.md;
  const [drawerEnabled, setDrawerEnabled] = useState(isLargeScreen);
  const toggleDrawerEnabled = () => !isLargeScreen && setDrawerEnabled((last) => !last);

  return (
    <STemplate>
      <SHeader>
        <DrawerButton clickable={!isLargeScreen} onClick={toggleDrawerEnabled}/>
        <span>Image Categories</span>
        <SProjectTitle>
            Devolon
        </SProjectTitle>
      </SHeader>
      <SWrapper drawerEnabled={drawerEnabled}>
        {drawerEnabled && <Sidebar />}
        <SContainer>
          {children}
        </SContainer>
      </SWrapper>
    </STemplate>
  );
}

export default Template;
