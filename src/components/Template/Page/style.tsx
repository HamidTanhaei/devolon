import styled from 'styled-components';
import {colors} from '../../../const';

export const SPageWrapper = styled.div`
  background: ${colors.white};
`

export const SHeader = styled.header`
    padding: 10px;
    border-bottom: 1px solid ${colors.gray};
    color: ${colors['gray-dark']};
`;

export const STitle = styled.h1`
  font-weight: normal;
  text-transform: uppercase;
  font-size: 20px;
  margin: 5px 0;
`;

export const SPageContainer = styled.div`
    padding: 10px;
`;
