import React from 'react';
import {SPageWrapper, SHeader, STitle, SPageContainer} from './style'

interface IProps{
    title: string;
    subtitle?: string;
}

const PageTemplate: React.FC<IProps> = ({children, title, subtitle}) => {
  return (
    <SPageWrapper>
      <SHeader>
        <STitle>{title}</STitle>
      </SHeader>
      <SPageContainer>{children}</SPageContainer>
    </SPageWrapper>
  );
}

export default PageTemplate;
