import React, {Suspense, lazy} from 'react';
import {BrowserRouter as Router, Route, Switch} from 'react-router-dom';
import {browserRoutes} from '../const'
import {useAppInitialization} from '../util/useAppInitialization';
import {routeMaker} from '../const/browserRoutes';
import Template from '../components/Template';

const Home = lazy(() => import('./Home'));
const ImageCategorySingle = lazy(() => import('./ImageCategorySingle'));
const PageNotFound = lazy(() => import('./PageNotFound'));

function Routes(props: any) {
  useAppInitialization();
  return (
    <Router>
      <Template>
        <Suspense fallback={'loading...'}>
          <Switch>
            <Route path={routeMaker(browserRoutes.home)} exact component={Home} />
            <Route path={routeMaker(browserRoutes.categorySingle)} exact component={ImageCategorySingle} />
            <Route component={PageNotFound} />
          </Switch>
        </Suspense>
      </Template>
    </Router>
  );
}

export default Routes;
