import React from 'react';
import styled from 'styled-components';
import {PageTemplate} from '../components';

const SWelcome = styled.div`
  text-align: center;
  padding: 10px 0;
`

function Home(props: any) {
  return (
    <PageTemplate title='Welcome to Cats Directory'>
      <SWelcome>
        Select a category from menu
      </SWelcome>
    </PageTemplate>
  );
}

export default Home;
