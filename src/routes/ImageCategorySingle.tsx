import React, {useCallback, useEffect, useState} from 'react';
import { RouteComponentProps } from 'react-router-dom';
import {useSelector} from 'react-redux';
import {getResources, getStatus} from 'redux-resource';
import styled from 'styled-components';

import {ImageCard, PageTemplate, Spinner} from '../components';
import {getImages} from '../redux/images';
import {IImage} from '../type/IImage';
import {breakpoints, colors, fontSize} from '../const';

interface MatchParams {
    id: string;
}
interface IProps extends RouteComponentProps<MatchParams>{}

const SImageList = styled.div`
  display: grid;
  grid-gap: 3px;
  grid-template-columns: auto auto auto;
  @media only screen and (min-width: ${breakpoints.md}px) {
    grid-gap: 10px;
    grid-template-columns: auto auto auto auto;
  }
  @media only screen and (min-width: ${breakpoints.lg}px) {
    grid-template-columns: auto auto auto auto auto auto;
  }
`

const STextCenter = styled.div`
    text-align: center;
`;

const SLoadMore = styled.button`
  cursor: pointer;
  margin-top: 20px;
  border-radius: 3px;
  background: ${colors.primary};
  padding: 10px 20px;
  color: ${colors.white};
  border: 0;
  font-size: ${fontSize.base};
`;

const ImageCategorySingle: React.FC<IProps>= (props) => {
  const [page, setPage] = useState(1);
  const categoryId = props.match.params.id;

  const loadImage = useCallback((page: number) => {
    getImages({categoryId: parseInt(categoryId), page: page}).then(() => {
      setPage(page);
    })
  }, [categoryId]);

  useEffect(() => {
    loadImage(1)
  }, [categoryId, loadImage]);

  
  // @ts-ignore
  const imageReduxData = useSelector((state) => state.images);
  const imageReduxDataStatus = getStatus(
    imageReduxData,
    `requests.getImage_${categoryId}_page${page}.status`,
    true
  );

  const images: IImage[] = getResources(imageReduxData, `cat_${categoryId}`);

  return (
    <PageTemplate title='Cats Images'>
      {imageReduxDataStatus.succeeded && (
        <SImageList>
          {images.map((image, key) => <ImageCard url={image.url} id={image.id} key={image.id} />)}
        </SImageList>
      )}
      {imageReduxDataStatus.pending && (
        <STextCenter>
          Loading ...
          <br />
          <Spinner />
        </STextCenter>
      )}
      <STextCenter>
        <SLoadMore onClick={() => loadImage(page + 1)}>Load More</SLoadMore>
      </STextCenter>
    </PageTemplate>
  );
}

export default ImageCategorySingle;
